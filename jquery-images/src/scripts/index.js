import "../styles/index.scss";

$(document).ready(() => {
  console.log("Hello jQuery");

  $("a#single_image").fancybox();

  $("a#inline").fancybox({
    hideOnContentClick: true
  });

  /**
   * Documentation for the slider
   * https://bxslider.com/examples/auto-show-start-stop-controls/
   */

  /**
   * Documentation for the Lightbox - Fancybox
   * See the section 5. Fire plugin using jQuery selector.
   * http://fancybox.net/howto
   */

  /**
   * Boostrap Modal (For the Learn More button)
   * https://getbootstrap.com/docs/4.0/components/modal/
   */
});

$(function() {
  $(".bxslider").bxSlider({
    mode: "fade",
    captions: true,
    slideWidth: 600
  });
});

/**
 * REMEMBER!!
 * Declaring Global functions that are accessible in your HTML.
 */

window.helloWorld = function() {
  console.log("HEllooOooOOo!");
};
